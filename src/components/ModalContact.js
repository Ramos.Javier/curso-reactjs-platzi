import React from 'react';
import { Table, Button, Containter, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter } from 'reactstrap';

class ModalContact extends React.Component {

    constructor(props) {
        super(props);
        this.state = { isOpen : this.props.show }
    }

    render() {
        return (
            <React.Fragment>
                <Modal isOpen={this.state.isOpen} >
                    <ModalHeader>Modal title</ModalHeader>
                    <ModalBody>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </ModalBody>
                    <ModalFooter>
                        <Button color='primary' onClick={this.toggle}>Do Something</Button>{' '}
                        <Button color='secondary' onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </React.Fragment>
        );
    }

}

export default ModalContact;