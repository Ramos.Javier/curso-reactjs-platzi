import React from 'react';

class Clock extends React.Component {

    constructor(props){
        super(props);
        this.state = { timeNow : new Date().toLocaleString() };
    }

    componentDidMount() {
        this.timerID = setInterval(
          () => this.tick(),
          1000
        );
      }
      
    componentWillUnmount() {
    clearInterval(this.timerID);
    }

    tick() {
        this.setState({ timeNow: new Date().toLocaleString() });
    }

    render() {
        return(
            <React.Fragment>
                <h4> {this.state.timeNow} </h4>
            </React.Fragment>
        );
    }

}

export default Clock;