import React from 'react'

class ComponentLifecycle extends React.Component {

    constructor(props) {
        super(props);
        console.log("constructor");

        this.state = { name : "test" }
    }

    componentDidMount() {
        console.log("mounted");
        this.setState({ name : "test_end" });
    }

    componentDidUpdate() {
        console.log("updated");
    }

    componentWillUnmount() {
        console.log("unmounted");
    }

    render() {  
        return(
            <p>Esto es ComponentLifecycle</p>
        );
    };

}

export default ComponentLifecycle