import React from 'react';
import { Link } from 'react-router-dom';
import ComponentLifecycle from '../components/ComponentLifecycle.js'
import Badge from '../components/Badge.js'

class PageOne extends React.Component {

    render() {
        return(
            <React.Fragment>
                <ComponentLifecycle/>
                <Link to="/page/two" className="btn btn-primary"> Ir a page 2</Link>
                <Badge 
                    firstName="Javier" 
                    lastName="Ramos"
                    avatarUrl="https://gitlab.com/uploads/-/system/user/avatar/1827454/avatar.png"
                    jobTitle="Frontend Engineer" 
                    twitter="sparragus"
                />
            </React.Fragment>
        )    
    }

}

export default PageOne;